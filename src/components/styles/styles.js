import { StyleSheet } from 'react-native';
import UserInformationColor from '../../constants/Colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  title: {
    marginTop: 5,
    justifyContent: 'center',
    fontFamily: 'Verdana',
    fontSize: 15,
    color: UserInformationColor.BLACK,
    alignItems: 'center',
    fontWeight: 'bold',
  },
});

export default styles;

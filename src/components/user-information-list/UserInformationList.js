import React, { useState, useEffect } from 'react';
import { FlatList, Text, View } from 'react-native';
import { styles } from '../../components/styles/styles';
import { EndPoint, HttpMethod} from '../../network/EndPoint';
import { ActivityIndicator } from "react-native";

function ShowUserInformation() { 

const { response, loading } = useAxios({
    method: HttpMethod.GET,
    url: EndPoint.FETCH_INFORMATION,
});
const [data, setData] = useState([]);

useEffect(() => {
    if (response !== null) {
        setData(response);
    }
}, [response]);

const renderRow = ({item}) => {
  return (
    <ListItem>
      <Text style={styles.title}>{item.name}</Text>
    </ListItem>
  );
};

return (
  <View style={styles.container}>
     {loading ? <ActivityIndicator/> : (
      <FlatList
        data={data}
        renderItem={renderRow}/>
      )}
  </View>
  );  
}
  
export default ShowUserInformation;
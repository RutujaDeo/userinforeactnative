const BASE_URL = 'https://jsonplaceholder.typicode.com';

const EndPoint = {
  FETCH_INFORMATION: { url: '/todos' },
};

const HttpMethod = {
  GET: 'get',
};

export { BASE_URL, EndPoint, HttpMethod };

import { createAppContainer } from 'react-navigation';
import { createStackNavigator} from 'react-navigation-stack';
import ShowUserInformation from './src/components/user-information-list/UserInformationList';

const App = createStackNavigator({
    UserInformationScreen: { screen: ShowUserInformation }, 
  },
  {
    initialRouteName: 'UserInformationScreen',
  }
);
export default createAppContainer(App);